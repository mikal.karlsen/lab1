package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords("chamn", "reality", "peace");
        isLeapYear(1996);
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        String Word1 = word1;
        int lengde1 = Word1.length();
        String Word2 = word2;
        int lengde2 = Word2.length();
        String  Word3 = word3;
        int lengde3 = Word3.length();

        if (lengde1 >= lengde2 & lengde1 >= lengde3)
            System.out.println(word1);
        if (lengde2 >= lengde1 & lengde2 >= lengde3)
            System.out.println(word2);
        if (lengde3 >= lengde1 & lengde3 >= lengde2)
            System.out.println(word3);

        // System.out.println(lengde1+" "+lengde2+" "+lengde3);
    }

    public static boolean isLeapYear(int year) {
        if (year % 400 == 0)
            return(true);
        if (year % 100 == 0)
            return(false);
        if (year % 4 == 0)
            return(true);
        return false;
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num % 2 == 0)
            {if (num >= 0) return (true);}
        return false;
    }

}

package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        int rowsum = 0;
        int colsum = 0;
        for (int i =0; i < grid.size(); i++){
            colsum += grid.get(i).get(0);
        }
        for (int i=0; i < grid.get(0).size(); i++){
            rowsum+=grid.get(0).get(i);
        }
        for (int i = 0; i<grid.size(); i++){
            int piss=0;
            for (int p=0; p<grid.get(0).size(); p++){
                piss += grid.get(i).get(p);
                }
            if (piss != rowsum){
                return false;
            }
        }
        for (int i=0; i<grid.get(0).size(); i++){
            int piss = 0;
            for (int p =0; p<grid.size(); p++){
                piss+=grid.get(p).get(i);
                }
            if (piss != colsum){
                return false;
                }
        }
        return true;
    }

}